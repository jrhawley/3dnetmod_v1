from plot_heatmap import plot_heatmap
#from plot_heatmap_two_community_sets_colored import plot_heatmap
from get_colormap import get_colormap
import numpy as np
from flatten_counts_single_region_geometric import flatten_counts_single_region
#from parse_community_calls_by_region import parse_community_calls_by_region
from parse_community_calls_by_region import parse_community_calls_by_region
#from parse_community_calls_by_region import parse_netmod_community_calls_by_region_merged
import sys
import os
from read_settings import read_settings
from counts_to_array import counts_to_array
from load_pixelmap import load_pixelmap
from load_region_start_end_coord import load_region_start_end_coord
from create_tags import create_tags
import glob

def get_zoom_region(zoom_coord,  region_coords):
	print 'zoom_coord: ', zoom_coord
	for region in region_coords:
		if zoom_coord['chrom'] == region_coords[region]['chrom']:
			if region_coords[region]['start'] < zoom_coord['start'] and region_coords[region]['end'] > zoom_coord['end']:
				print 'found region: ', region
				return region

	print 'DID NOT FIND REGION!'
	return None


def load_loci(file, qvalue_threshold, window):
	input = open(file, 'r')
	loci = []
	for line in input:
		pieces = line.strip().split(',')
		if pieces[0][:3] == 'chr':
			print 'pieces'
			print pieces

			locus = {}
			locus['chrom'] = pieces[0]
			locus['coord'] = int(pieces[1])
			locus['start'] = int(pieces[1]) - window/2
			locus['end'] = int(pieces[1]) + window/2
			locus['qvalue'] = float(pieces[-1])
			print 'qvalue'
			print locus['qvalue']
			if locus['qvalue'] < qvalue_threshold: 
				loci.append(locus)

	#print loci
	print 'number of loci: ', len(loci)
	return loci



def main():
	settings = read_settings(sys.argv[1])
	tags = create_tags(sys.argv[1])
	tag_preprocess = tags[0]
	tags_HSVM = tags[3]
	loci_file = sys.argv[2]
	window = int(sys.argv[3])
	boundary_category = sys.argv[4]
	qvalue = 0.1
	loci = load_loci(loci_file, qvalue, window)
	dir = 'output/DBR/heatmaps_low_qvalues/'
	if not os.path.isdir(dir): os.makedirs(dir)
	#linewidth = 3


	WT_CTCF = 'input/chip/WT_CTCF.bw'
	KO_CTCF = 'input/chip/KO_CTCF.bw'

	samples = [settings['sample_1'], settings['sample_3']]
	for cell_type in samples:
		#community_file = 'output/FINAL_DOMAIN_CALLS/' + cell_type + '_master_consistent_domains.txt'
		community_file = 'output/FINAL_DOMAIN_CALLS/' + cell_type + '_' + settings['scale'] + '_' + tags_HSVM +  '_master_consistent_domains.txt'
		for locus in loci:
			chr = locus['chrom']
			counts_files = glob.glob('input/' + chr + '.*' + '_' + cell_type + '_' + tag_preprocess + 'finalpvalues.counts')
			for counts_file in counts_files:
				bed_filename = 'input/' + counts_file.split('/')[-1][:-14] + '.bed'
				subchrom = bed_filename.split('/')[-1].split('_')[0]
				region_coords, bin_size = load_region_start_end_coord(bed_filename)
				zoom_region = get_zoom_region(locus, region_coords)
				region = zoom_region
				if region != None:
					str_zoom = locus['chrom'] +  ':' + str(locus['start']) + '-' + str(locus['end'])
					zoom_window = (str_zoom, str_zoom)
					counts,_ = counts_to_array(counts_file,bed_filename)
					pixelmap = load_pixelmap(bed_filename)
					start_coord = pixelmap[region][0]['start']
					end_coord = pixelmap[region][-1]['end']
					colorscale=(0.0,np.percentile(flatten_counts_single_region(counts[region], discard_nan=True),98))
					communities = parse_community_calls_by_region(community_file, chr, start_coord, end_coord)
					heatmap_filename = dir + cell_type + '_' + boundary_category + '_' + str(locus['qvalue']) + '_' + chr + '_' + str(locus['coord']) + '_' + str(window/1000000) + 'Mbwindow.png'
					#plot_heatmap(counts[zoom_region], heatmap_filename, zoom_window = zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track ='input/gene_tracks/empty_track.txt', tracks = [KO_CTCF, WT_CTCF], track_scales = "auto-pairs", show_track_labels = True,chipseq_track_line_width=linewidth)
					plot_heatmap(counts[zoom_region], heatmap_filename, zoom_window = zoom_window,colorscale = colorscale, show_colorscale = True, cmap = get_colormap('obs'), region = region, communities = communities, primer_file = (bed_filename), gene_track = 'input/gene_tracks/mouse_mm10_genes.bed', tracks = [KO_CTCF, WT_CTCF], track_scales = "auto-pairs", show_track_labels = False)





if __name__ == '__main__':
	main()
	
