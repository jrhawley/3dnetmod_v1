from convert_comm_coord_to_array import convert_comm_coord_to_array

def convert_community_list_to_partition_list(community_list, bin_location, region):
        partitions_list = []
        for community in community_list:
                if region == community['region']:
                        community_array = convert_comm_coord_to_array(community, bin_location)
			print 'length of comparison commuinity array: ', len(community_array[0])
                        partitions_list.append(community_array)
        return partitions_list
