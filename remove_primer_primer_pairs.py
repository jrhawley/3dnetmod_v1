from math import ceil

import numpy as np


def remove_primer_primer_pairs(counts_superdict, primermap, count_threshold=10.,
                               num_reps=None, max_reps=None,
                               percentage_reps=None, fraction_reps=None,
                               all_reps=False, inplace=False):
    """
    Removes primer-primer pairs from a set of replicates according to criteria
    specified by the kwargs, wiping them with ``np.nan``.

    Legacy code inherited from
    https://bitbucket.org/creminslab/primer-primer-pair-remover

    Parameters
    ----------
    counts_superdict : Dict[str, Dict[str, np.ndarray]]
        The counts superdict data structure to remove primer-primer pairs from.
        The outer keys should be the replicate names, the inner keys should be
        the region names, and the values should be the square, symmetric
        matrices of counts values.
    primermap : Dict[str, List[Dict[str, Any]]]
        The primermap or pixelmap describing the loci whose interaction
        frequencies are quantified in the ``counts_superdict``. The keys of the
        outer dict should be region names. The values should be lists, where
        the :math:`i` th entry represents the :math:`i` th primer in that
        region. Primers are represented as dicts with the following structure::

            {
                'chrom' : str,
                'start' : int,
                'end'   : int
            }

    count_threshold : float
        A rep passes the threshold if it is greater than or equal to this
        number, and fails the threshold if it is less than this number.
    num_reps : Optional[int]
        If fewer than ``num_reps`` reps fail, the primer-primer pair will not be
        wiped. You can pass ``fraction_reps`` instead of this kwarg.
    max_reps : Optional[int]
        If ``max_reps`` or more reps fail, the primer-primer pair will not be
        wiped. If this kwarg is not passed then this condition is not applied.
    fraction_reps : Optional[float]
        Pass a fraction (between 0 and 1) as a float to make the condition be
        that this fraction of the reps must clear the threshold.
    percentage_reps : Optional[float]
        You can pass ``fraction_reps`` as a percentage here (to support legacy
        code).
    all_reps : bool
        Pass True to make the condition be that the sum across all replicates
        must clear the threshold. This is the default mode if niether
        ``num_reps`` nor ``percentage_reps`` is passed. Pass False to apply the
        threshold to each replicate individually and judge the number of failed
        replicates against ``num_reps`` and ``max_reps``.
    inplace : bool
        Pass True to operate in-place on the passed ``counts_superdict``; pass
        False to return a new counts superdict.

    Returns
    -------
    Dict[str, Dict[str, np.ndarray]]
        The result of the primer-primer pair removal, in the form of a counts
        superdict data structure analagous to the ``counts_superdict`` passed to
        this function. The outer keys are the replicate names, the inner keys
        are the region names, and the values are the processed counts matrices.
    """
    print 'in primer primer pair removal'
    # honor inplace
    if inplace:
        new_counts_superdict = counts_superdict
    else:
        new_counts_superdict = {
            rep: {
                region: counts_superdict[rep][region].copy()
                for region in counts_superdict[rep]}
            for rep in counts_superdict}

    # percentage_reps legacy support
    if percentage_reps is not None:
        fraction_reps = percentage_reps / 100.

    # resolve num_reps vs fraction_reps
    if num_reps is None and fraction_reps is not None:
        num_reps = int(ceil(len(new_counts_superdict) * fraction_reps))

    # fall back to all_reps
    if num_reps is None and fraction_reps is None:
        all_reps = True

    # set nan's
    #for region in primermap:
    #for region in ['chr1', 'chr2', 'chr3']:
    for region in counts_superdict[counts_superdict.keys()[0]]:
        print 'region in counts_superdict: ', region
        if all_reps:
            # all_reps mode: sum values over all reps
            replicate_sum = sum([new_counts_superdict[rep][region]
                                 for rep in new_counts_superdict])
            # check the sum against the threshold
            failed = replicate_sum < count_threshold
        else:
            # per-rep mode: check each rep for failure
            rep_failed = {
                rep: new_counts_superdict[rep][region] < count_threshold
                for rep in new_counts_superdict
            }
            # count the total number of failing reps
            num_failed = sum([rep_failed[rep]
                              for rep in new_counts_superdict])
            # check total number of failures against num_reps/max_reps
            if max_reps is not None:
                failed = max_reps > num_failed >= num_reps
            else:
                failed = num_failed >= num_reps
        for rep in new_counts_superdict:
            new_counts_superdict[rep][region][failed] = np.nan

    return new_counts_superdict
