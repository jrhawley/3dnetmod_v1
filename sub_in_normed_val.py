import numpy as np


def sub_in_normed_val(data, ranks, average_col, tied_rank_idx, tie):
    """
    Returns data matrix with normalized values. Handles ties according to the
    value passed in the ``tie`` kwarg.

    Parameters
    ----------
    data : list of list of numeric
        The column-sorted data to sub in values for.
    ranks : 2d array
        The original ranks of the values in ``data``.
    average_col : 1d array
        The average of each row of data.
    tied_rank_idx : list of list of numeric
        The indices that separate runs of ranks that have the same values.
    tie : {'lowest', 'average'}, optional
        Pass ``'lowest'`` to set all tied entries to the value of the lowest
        rank. Pass ``'average'`` to set all tied entries to the average value
        across the tied ranks.

    Returns
    -------
    list of 1d array
        The normalized data.
    """
    sorted_norm_data = np.zeros((len(data), len(data[0])))
    normed_data = []
    for col_num in range(0, len(data)):
        # find all values of shared rank
        for rank_num in range(len(tied_rank_idx[col_num])-1):
            start_idx = tied_rank_idx[col_num][rank_num]
            end_idx = tied_rank_idx[col_num][rank_num + 1]
            if tie == 'lowest':
                # give all of these entries the value of lowest rank
                sorted_norm_data[col_num][start_idx:end_idx] =\
                    average_col[start_idx]
            else:  # tie type is average
                # give all of these entries average value of their ranks
                average_val = np.mean(average_col[start_idx:end_idx])
                sorted_norm_data[col_num][start_idx:end_idx] = average_val
        # sort data back to original order
        rev_sort_ord = np.array(ranks[col_num], dtype='int32')
        normed_data.append(sorted_norm_data[col_num][rev_sort_ord])

    return normed_data
