import numpy as np


def average_rows(data, reference_index=None):
    """
    Find average value for each row of sorted data.

    Parameters
    ----------
    data : list of 1d numpy array
        The data to average.
    reference_index : Optional[int]
        Pass an index into the outer list of ``data`` to serve as the reference
        distribution instead of averaging.

    Returns
    -------
    1d array
        The average of each row of data.
    """
    data_mat = np.zeros((len(data[0]), len(data)))
    # put data into numpy 2-D array
    for col_num in range(0, len(data)):
        data_mat[:, col_num] = data[col_num]
    # find average of rows
    if reference_index is None:
        average_col = np.mean(data_mat, 1)
    else:
        average_col = data_mat[:, reference_index]
    return average_col
