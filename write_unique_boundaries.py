def write_unique_boundaries(in_file, out_file):
        input = open(in_file, 'r')
        unique_boundaries = []
        boundary_ids = []
        for line in input:
                pieces = line.strip().split('\t')
                if pieces[0].startswith('chr'):
                        chr = pieces[0]
                        start = pieces[1]
                        end = pieces[2]
                        boundary_id1 = chr + '_' + start
                        boundary_id2 = chr + '_' + end
                        if boundary_id1 not in boundary_ids:
                                unique_boundaries.append((chr, start))
                                boundary_ids.append(boundary_id1)
                        if boundary_id2 not in boundary_ids:
                                unique_boundaries.append((chr, end))
                                boundary_ids.append(boundary_id2)

	output = open(out_file, 'w')
	for boundary in unique_boundaries:
		temp = boundary[0] + '\t' + boundary[1]
		print >> output, temp
	output.close()



