

def write_domain_boundaries(domain_boundaries, outfile):
    with open(outfile, 'w') as handle:
        for chrom in domain_boundaries:
            for coord in domain_boundaries[chrom]:
                handle.write('%s\t%i\n' % (chrom, coord))
